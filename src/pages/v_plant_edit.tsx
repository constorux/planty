import { Diameter, Flower2, Leaf, ShrubIcon, Trash2, TreeDeciduous, XIcon } from "lucide-react";
import { Field, showConfirmDialog } from "../util";
import { DataService, Plant } from "../service/s_data";
import { useSignal, useSignalEffect } from "@preact/signals";

export function PlantEditView({ plant, onClose }: { plant: any, onClose: () => void}) {
    return (
      <div class=" column cross-stretch-fill padded" >
        <div class="card column cross-stretch raised edit-view">
          <div class="row main-space-between">
            <Field
              value={plant.name}
              label="name"
              placeholder={""}
              onSubmit={(d) => {
                DataService.i.setPlant( {
                  ...plant,
                  name: d,
                });
              }}
            />
  
            <button class="integrated" onClick={() => onClose()}>
              <XIcon />
            </button>
          </div>
            <PlantTypeEditView plant={plant} />
          <_StateView plant={plant} />
          <_SizeSlider plant={plant} />
          <Field
            value={plant.comment}
            label="comment"
            placeholder={""}
            multiline={true}
            onSubmit={(d) => {
                DataService.i.setPlant({
                    ...plant,
                    comment: d,
                });
            }}
          />
          <button class="action" onClick={async () => {
              const d = await showConfirmDialog({title: "Are you sure you want to delete this plant?", message: "This action cannot be undone"});
              if(d){ DataService.i.deletePlant(plant.id);
                  onClose();
              }
          }}>
            <Trash2 />
            delete
          </button>
        </div>
      </div>
    );
  }


  export const plantTypeIcons = {
    tree: <TreeDeciduous/>, bush: <ShrubIcon/>, hedge: <Leaf/>, plant: <Flower2/>};


  export function PlantTypeView({ plant }: { plant: Plant }) {
    return plantTypeIcons[plant.type] ?? <div/>;
  }


  export function PlantTypeEditView({ plant,  }: { plant: Plant }) {
    return (
      <div
        class="card row padding-none gap-none" 
        style="overflow: hidden">
        {Object.keys(plantTypeIcons).map((t) => (
          <button
            class={"flex-1 " + (t == plant.type ? "loud" : "action")}
            style="border-radius: 0"
            onClick={() =>
              DataService.i.setPlant({
                ...plant,
                type: t as any,
              })
            }
          >
            {plantTypeIcons[t]}
          </button>
        ))}
      </div>
    );
  }

  function _StateView({ plant }: { plant: Plant }) {
   return <div
            class="card row padding-none gap-none"
            style="overflow: hidden">
                {["growing", "removed", "died"].map((s) => (
               
            <button class={"flex-1 " + (s == plant.state ? "loud" : "action")} style="border-radius: 0" onClick={
                () => DataService.i.setPlant( {...plant, state: s as any})
                
            }>
              {s}
            </button>))}
          </div>}

  
  function _SizeSlider({ plant }: { plant: Plant }) {
    let rSig = useSignal(null);
  
    return (
      <div id={"plant_" + plant.id} class="row" style="margin: 1rem 0">
        <Diameter style={"margin-left:0.9rem"} />
        <label style="text-align: end" class="flex-1">
          {rSig.value ?? plant.radius * 2}m
        </label>
        <input
          class="flex-4"
          type="range"
          value={rSig.value ?? plant.radius * 2}
          min={0.2}
          max={4}
          step={0.1}
          onInput={(e) => (rSig.value = Number.parseFloat(e.currentTarget.value))}
          onMouseUp={(e) =>
            DataService.i.setPlant({
              ...plant,
              radius: Number.parseFloat(e.currentTarget.value) / 2,
            })
          }
        />
      </div>
    );
  }
  