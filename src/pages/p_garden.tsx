import { Signal, useSignal } from "@preact/signals";
import { ProjectBit } from "../bit/b_garden";
import { PlantsBit } from "../bit/b_plants";
import { DataService, Plant, Project } from "../service/s_data";
import { Field, showConfirmDialog, showToast } from "../util";
import {
  Diameter,
  Eye,
  EyeOff,
  Flower2,
  Leaf,
  LeafyGreen,
  Plus,
  Ruler,
  ShrubIcon,
  Trash2,
  TreeDeciduous,
  Trees,
  XIcon,
} from "lucide-react";
import { PlantEditView } from "./v_plant_edit";
import { PlantsView } from "./v_plants";

export function GardenPage({ id }: { id: string }) {
  return (
    <ProjectBit.Provide _id={id}>
      <div class="column">
        <_GardenPage />
      </div>
    </ProjectBit.Provide>
  );
}

function _GardenPage() {
  const gardenBit = ProjectBit.use();
  return gardenBit.map({
    onData: (d) => (
      <PlantsBit.Provide project={d.id}>
        <_PlantsView garden={d} />
      </PlantsBit.Provide>
    ),
  });
}

function _PlantsView({ garden }: { garden: Project }) {
  const showSig = useSignal(false);
  const selSig = useSignal<number | null>(null);
  const plantsBit = PlantsBit.use();
  return plantsBit.map({
    onData: (d) => (
      <div
        class="row-resp-rev gap-3 cross-start"
        style="width:100%; padding: 1rem"
      >
        <ActionsView project={garden} showSig={showSig} />
        <div
          class="edit-pane"
          style={"width: " + (selSig.value != null ? "350px" : "0")}
        >
          {selSig.value != null ? (
            <PlantEditView
              plant={d[selSig.value]}
              onClose={() => (selSig.value = null)}
            />
          ) : (
            <div />
          )}
        </div>
        <PlantsView
          garden={garden}
          plants={d}
          selSig={selSig}
          showSig={showSig}/>
      </div>
    ),
  });
}



function ActionsView({
  project,
  showSig,
}: {
  project: Project;
  showSig: Signal<boolean>;
}) {
  return (
    <div
      class="column-resp column cross-end"
      style="position: fixed; right: 1rem; bottom: 1rem; z-index: 100"
    >
      <button
        class="loud minor"
        onClick={() => {
          showSig.value = !showSig.value;
        }}
      >
        {showSig.value ? <EyeOff /> : <Eye />}
        <span class="if-wide">
          {showSig.value ? "hide removed" : "show all"}
        </span>
      </button>
      <button
        class="loud minor"
        onClick={() => {
          DataService.i.setPlant({
            project: project.id,
            name: "Plant",
            x_pos: 1,
            y_pos: 1,
            radius: 0.5,
            type: "tree",
            state: "growing",
            comment: "",
          });
        }}
      >
        <Plus />
        <span class="if-wide">add</span>
      </button>
    </div>
  );
}
