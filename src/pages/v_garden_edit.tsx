import { Delete, Edit2, ImageIcon, Save, Trash2 } from "lucide-react";
import { DataService, Project } from "../service/s_data";
import { Signal, useSignal } from "@preact/signals";
import { chooseImageFile, showConfirmDialog } from "../util";
import { ElbeDialog } from "../elbe/components";

export function GardenEdit({
  project,
  openSig,
  onAction,
}: {
  project: Project;
  openSig: Signal<boolean>;
  onAction: () => void;
}) {
  const pSig = useSignal({ ...project });

  async function save() {
    openSig.value = false;
    await DataService.i.setProject(project.id,{ ...pSig.peek(), floorplan: undefined });
    onAction();
  }

  async function del() {
    openSig.value = false;
    const v = await showConfirmDialog({
      title: "delete garden",
      message: "are you sure you want to delete this garden?",
    });
    if (!v) return;
    await DataService.i.deleteProject(project.id);
    onAction();
  }

  return (
    <div class="column cross-stretch-fill" style="width: min(90vw,19rem)">
      <input
        type="text"
        value={pSig.value.name}
        placeholder="name"
        onChange={(v) => {
          pSig.value.name = (v.target as any).value;
        }}
      />
      <input type="text" value={pSig.value.width} placeholder="width (in m)" />
      <textarea value={pSig.value.about} placeholder="about"  onChange={(v) => {
          pSig.value.about = (v.target as any).value;
        }} />
      <div class="row cross-end">
        {project.id ? (
          <button class="action" onClick={del}>
            <Trash2 />
          </button>
        ) : null}
        <button class="loud flex-1 row" onClick={save}>
          <Save />
          save
        </button>
      </div>
    </div>
  );
}

export function ImageEditDialog({ project, onAction }: { project: Project, onAction: () => void}) {
  const imageSignal = useSignal<File>(null);

  function getImage() {
    chooseImageFile().then((f) => (f == null ? null : (imageSignal.value = f)));
  }

  const openSig = useSignal(false);
  return (
    <div onClick={(e) => e.stopPropagation()}>
      <button class="integrated" onClick={() => (openSig.value = true)}>
        <ImageIcon />
      </button>
      <ElbeDialog
        open={openSig.value}
        title="edit image"
        onClose={() => (openSig.value = false)}
      >
        <div class="column cross-stretch-fill" style="width: min(90vw, 19rem)">
          <img
            src={
              imageSignal.value
                ? URL.createObjectURL(imageSignal.value)
                : project.floorplan
            }
            style="max-height: 15rem; object-fit: contain"
            class="pointer"
            onClick={getImage}
          /> {imageSignal.value ? (
          <button
            class="loud"
            onClick={async (e) => {
              if (imageSignal.peek() == null) return;
              openSig.value = false;
              const fd: FormData = new FormData();
              fd.append("floorplan", imageSignal.peek() as any);

              await DataService.i.setProject(project.id, fd);
                onAction();
            }}
          >
            save
          </button>) : null}
        </div>
      </ElbeDialog>
    </div>
  );
}
