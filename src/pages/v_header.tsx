import { LogOut, User } from "lucide-react";
import { route } from "preact-router";
import { AuthBit } from "../bit/b_auth";
import { Rob3in } from "../elbe/components/rob3in";

export function HeaderView() {


  return (
    <div>
      <div style="height: 5rem"></div>
      <div class="header">
        <div class="header-title flex-1 cross-start" onClick={goHome}>
          
            <div class="row gap-none svg-filled" style="font-weight: normal">
              <b class="" style="font-family: 'Space Mono';">
                garten
              </b>
              <span class="action b">•</span>
              <Rob3in height="0.87rem" />
            </div>
         
        </div>
        <ProfileButton />
      </div>
    </div>
  );
}



function goHome() {
  route("/");
}

function ProfileButton() {
  const authBit = AuthBit.use();

  return authBit.map({
    onLoading: () => <div/>,
    onError: (e) => <div/>,
    onData: (auth) => auth ? (
    <button class="integrated" onClick={() => authBit.ctrl.logout()}>
      <div class="if-wide">{auth.username ?? ""}</div>
      <LogOut />
    </button>
  ) : null});
}
