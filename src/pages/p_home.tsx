import { Edit2, Plus } from "lucide-react";
import { AuthBit } from "../bit/b_auth";
import { DataService, Project } from "../service/s_data";
import { go } from "../util";
import { SBit } from "../util/bit/sbit";
import { useSignal } from "@preact/signals";
import { ElbeDialog } from "../elbe/components";
import { GardenEdit, ImageEditDialog } from "./v_garden_edit";

export function HomePage({}) {
  const authBit = AuthBit.use();
  return authBit.map({ onData: (user) => <_HomePage id={user.id} /> });
}

export function _HomePage({ id }: { id: string }) {
  const f = async () => await DataService.i.getProjects(id);
  const { map,ctrl } = SBit(f);

  return (
    <div class="base-limited column cross-stretch-fill">
      <h2>my gardens</h2>
      {map.value({
        onData: (projects) => (
          <div class="column cross-stretch-fill">
            {projects.map((project) => (
              <ProjectSnippet project={project} reload={ctrl.reload}/>
            ))}
            <button
                class="action"
                onClick={async () => {
                    await DataService.i.setProject(null,{user: id, name: "new garden", about: "", width: 10});
                    ctrl.reload();
                }}
              ><Plus/>
                new garden
              </button>
          </div>
        ),
      })}
    </div>
  );
}

function ProjectSnippet({ project,reload }: { project: Project, reload: () => void}) {
  const openSig = useSignal(false);

  return (
    <div>
    <div
      class="card row main-space-between pointer"
      onClick={go("/garden/" + project.id)}
    >
      <div class="column cross-stretch-fill">
        <span class="header-5">{project.name}</span>
        <span>{project.about}</span>
      </div>
      <ImageEditDialog project={project} onAction={reload} />
      <button class="integrated" onClick={(e) => {
        e.stopPropagation();
        openSig.value = true;}}>
        <Edit2 />
      </button>
      
    </div>
    <ElbeDialog
        open={openSig.value}
        title={project.id ? "edit garden" : "new garden"}
        onClose={() => (openSig.value = false)}
      >
       
       <GardenEdit  project={project} openSig={openSig} onAction={reload}/>
      </ElbeDialog>
    </div>
  );
}
