import { useState } from "preact/hooks";
import { AuthBit } from "../bit/b_auth";
import { Spinner } from "..";
import { TriangleAlert } from "lucide-react";

export function LoginView({}){
    const authBit = AuthBit.use();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);

    return authBit.map({
        onLoading: () => <Spinner/>,
        onError: (e) => <div>Error: {e}</div>,
        onData: (auth) => auth ? (
            null
        ) : (
            <div class="base-limited column cross-stretch-fill" style="max-width: 300px">
                <h1 style="text-align: center">Login</h1>
                <input type="text" placeholder="username" value={username} onInput={(e) => setUsername(e.currentTarget.value)}/>
                <input type="password" placeholder="password" value={password} onInput={(e) => setPassword(e.currentTarget.value)}/>
                <button onClick={() => { 
                    setError(null);
                    authBit.ctrl.login({username, password}).catch((_) => setError("login failed"));}}>login</button>
                {error ? <div class="card row secondary">
                    <TriangleAlert/>
                    {error}</div> : null}
            </div>
        )
    });
}