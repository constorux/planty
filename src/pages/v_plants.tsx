
import { Signal, useSignal } from "@preact/signals";
import { DataService, Plant, Project } from "../service/s_data";
import { showToast } from "../util";
import { ListIcon, MapIcon } from "lucide-react";
import { PlantTypeView, plantTypeIcons } from "./v_plant_edit";

export function PlantsView({ garden, plants, selSig, showSig }: {
    garden: Project;
    plants: Plant[];
    selSig: Signal<number | null>;
    showSig: Signal<boolean>;
  
  }){

    const viewModeSig = useSignal<"map" | "list">("map");

  return <div class="flex-1 column main-center padded noselect" style="">
    <button class="action" onClick={
        () => {
          viewModeSig.value = viewModeSig.value == "map" ? "list" : "map";
        }
      
    }>
        {viewModeSig.value === "list" ? (
            <div class="row">

<MapIcon />
view as map
            </div>
        ) : <div class="row">

        <ListIcon />
        view as list
                    </div>}
    </button>
            {viewModeSig.value == "map" && <_PlantsImgView
              garden={garden}
              plants={plants}
              selSig={selSig}
              show={showSig.value}
            />}

            {viewModeSig.value == "list" && <_PlantsListView
              plants={plants}
                showRemoved={showSig.value}
                selected={selSig.value}
              onSelected={(i) => {
                selSig.value = i;
              }}
            />}
          </div>
  }
  
  function _PlantsImgView({
    garden,
    plants,
    selSig,
    show,
  }: {
    garden: Project;
    plants: Plant[];
    selSig: Signal<number | null>;
    show: boolean;
  }) {
    return (
      <div style="position: relative; height: auto; width: min(100%, 700px)">
        <img src={garden.floorplan} style="width:100%" />
        {plants.map((p, i) =>
          p.state == "growing" || show ? (
            <_PlantView
              key={p.id}
              plant={p}
              selected={selSig.value == i}
              width={garden.width}
              onTapped={() => (selSig.value = i)}
            />
          ) : null
        )}
      </div>
    );
  }
  
  function _PlantsListView({

    plants,
    showRemoved,
    selected,
    onSelected,
  }: {
    plants: Plant[];
    showRemoved: boolean;
    selected: number | null;
    onSelected: (i: number) => void;
  }) {
    const filterSig = useSignal({type: null});

    return (
      <div class="column cross-stretch-fill" 
      style="max-width: min(90%, 30rem)"
      >
        <div class="row" style="margin-top: 2rem">
        {Object.entries(plantTypeIcons).map(([type, icon]) => (
            <button
                class={(filterSig.value.type == type ? "loud" : "action")
                }
                onClick={() => {
                filterSig.value = { type: filterSig.value.type == type ? null : type};
                }}
            >
                {icon}
            </button>
        ))}
        </div>
        {plants
        .filter(p => filterSig.value.type == null || p.type == filterSig.value.type)
        .filter(p => showRemoved || p.state == "growing")
        .map((p, i) => (
            <div class={`column card cross-stretch ${selected == i && "secondary"}`} onClick={() => onSelected(i)}>
          <div
            class="row-resp"
            
          >
            <div class="flex-1 header-6">{p.name}</div>
            <PlantTypeView plant={p} />
          </div>
            <div>{p.comment}</div>
            <div style="font-family: monospace">{p.x_pos.toFixed(1)}x {p.y_pos.toFixed(1)}y</div>
          </div>
        ))}
      </div>
    );
  }


function _PlantView({
    plant,
    width,
    onTapped,
    selected,
  }: {
    plant: any;
    width?: number;
    onTapped: () => void;
    selected?: boolean;
  }) {
    const pS = useSignal({ x: plant.x_pos, y: plant.y_pos });
  
    function handleDragStart(e) {
      const img = new Image();
      e.dataTransfer.setDragImage(img, 0, 0);
    }
  
    function drag(e, emit: boolean) {
      _drag(e, e.clientX, e.clientY, emit);
    }
  
    function touchDrag(e, emit: boolean) {
      _drag(e, e.touches[0].clientX, e.touches[0].clientY, emit);
    }
  
    function updateLocation(x: number, y: number) {
      DataService.i.setPlant({
        ...plant,
        x_pos: x,
        y_pos: y,
      });
      showToast("Plant moved");
    }
  
    function _drag(e, x, y, emit: boolean) {
      e.preventDefault();
      e.stopPropagation();
      const parentRect = (
        e.target as HTMLElement
      ).parentElement.getBoundingClientRect();
      const relX = (x - parentRect.left) / parentRect.width;
      const relY = (y - parentRect.top) / parentRect.height;
      pS.value = { x: relX * width, y: relY * width };
      if (emit) updateLocation(relX * width, relY * width);
    }
  
    return (
      <div
        class={"vplant " + plant.type + " " + (selected ? "selected" : "")}
        title={plant.name}
        draggable={true}
        onClick={() => onTapped()}
        onDrop={(e) => e.preventDefault()}
        onDragStart={handleDragStart}
        onTouchMove={(e) => touchDrag(e, false)}
        onTouchEnd={(e) => {
          if (pS.value.x == plant.x_pos && pS.value.y == plant.y_pos) return;
          updateLocation(pS.value.x, pS.value.y);
        }}
        onDragEnd={(e) => drag(e, true)}
        onDragOver={(e) => drag(e, false)}
        style={{
          left: `${(pS.value.x / width) * 100}%`,
          top: `${(pS.value.y / width) * 100}%`,
          width: `${(plant.radius / width) * 2 * 100}%`,
          touchAction: "none",
        }}
      />
    );
  }