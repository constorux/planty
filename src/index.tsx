import { Component, render } from "preact";
import "../style/elbe/elbe.scss";
import "../style/google.scss";
import "../style/base.scss";
import { Loader2 } from "lucide-react";
import { Route, Router, route } from "preact-router";
import { useEffect } from "preact/hooks";
import { AuthBit } from "./bit/b_auth";
import { AuthService, AuthState } from "./service/s_auth";
import { HeaderView } from "./pages/v_header";
import { FooterView } from "./pages/v_footer";
import { HomePage } from "./pages/p_home";
import { LoginView } from "./pages/p_login";
import { GardenPage } from "./pages/p_garden";

export const appInfo = {
	name: "garten",
	version: "0.0.7",
	repo: "https://gitlab.com/constorux/planty",
};

function _Router({}) {
  return (
    <Router>
      <Route path="/login" component={LoginView} />
      <ProtectedRoute path="/" component={HomePage} />
      <ProtectedRoute path="/garden/:id" component={GardenPage} />
    </Router>
  );
}

class App extends Component {
  async componentDidMount() {
    // Check if user is authenticated here
    // You can call the guard method here
    this.guard(window.location.pathname, await AuthService.i.get());
  }

  // some method that returns a promise
  guard(url: string, user: AuthState) {
    if (user) return;
    route("/login", true);
  }

  render() {
   
    return  <div>
          <HeaderView />
         
            <_Router />
          
          <FooterView />
          <div style="height:0px width: 0px; border: solid 1px transparent"></div>
        </div>
     
  }
}

function Root() {
  return (
    <div class="content-base elbe-base primary">
      <AuthBit.Provide>
        <App />
      </AuthBit.Provide>
    </div>
  );
}

export function Spinner() {
  return (
    <div style="margin: 5rem 0" class="centered padded">
      {" "}
      <div class="rotate-box">
        <Loader2 />
      </div>
    </div>
  );
}

function ProtectedRoute(props: any) {
  const { map } = AuthBit.use();

  const isLoggedIn = map({
    onData: (d) => !!d,
    onError: (e) => false,
    onLoading: () => false,
  });

  useEffect(() => {
    if (!isLoggedIn) {
      route("/", true);
    }
  }, [isLoggedIn]);

  if (!isLoggedIn) return null;
  return <Route {...props} />;
}

render(<Root />, document.getElementById("app"));
