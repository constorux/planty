import { DataService, Project } from "../service/s_data";
import { CtrlBit, WorkerControl } from "../util/bit/ctrl_bit";



type Inputs = { _id: string };
type Data = Project;

class Ctrl extends WorkerControl<Inputs, Data> {
  async worker() {
    return  DataService.i.getProject(this.p._id);
  }
}

export const ProjectBit = CtrlBit<Inputs, Data, Ctrl>(
  (p, b) => new Ctrl(p, b),
  "Project"
);
