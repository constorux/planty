
import { route } from "preact-router";
import { AuthService, AuthState } from "../service/s_auth";
import { go } from "../util";
import { CtrlBit, StreamControl } from "../util/bit/ctrl_bit";

type Inputs = {};
type Data = AuthState;

class Ctrl extends StreamControl<Inputs, Data> {
  async listen() {
    AuthService.i.observe(
      (u) => this.bit.emit(u),
      (e) => this.bit.emitError(e)
    );
  }
  logout = async () => {
    this.bit.emitLoading();
    await AuthService.i.logout();
    route("/login");
  };

  login = async (d:{username:string, password:string} ): Promise<void> => {
    //this.bit.emitLoading();
     await AuthService.i.login(d);
     route("/");
    
  };
}

export const AuthBit = CtrlBit<Inputs, Data, Ctrl>(
  (p, b) => new Ctrl(p, b),
  "Auth"
);
