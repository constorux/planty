import { DataService, Plant, Project } from "../service/s_data";
import { CtrlBit, StreamControl, WorkerControl } from "../util/bit/ctrl_bit";



type Inputs = { project: string };
type Data = Plant[];

class Ctrl extends StreamControl<Inputs, Data> {
  listen(): Promise<void> {
    return DataService.i.observePlants(this.p.project, (d) => this.bit.emit(d));
  }

  set(plant: Plant) {DataService.i.setPlant(plant);}
  
}

export const PlantsBit = CtrlBit<Inputs, Data, Ctrl>(
  (p, b) => new Ctrl(p, b),
  "Plants"
);
