import { go } from "../util";
import { pb } from "./s_data";


interface AuthUser {
  id: string;
  username?: string;
  email: string;
}

export type AuthState = AuthUser | null;

export class AuthService {
  public static i = new AuthService();
  private constructor() {}

  async observe(
    data: (d?: AuthState) => void,
    error: (e: string) => void
  ): Promise<void> {
    pb.authStore.onChange(async (_) => data(await AuthService.i.get()));
    data(await AuthService.i.get());
  }

  async get(): Promise<AuthState> {
    if (!pb.authStore.isValid) return null;
    pb.authStore.isValid;
    return { id: pb.authStore.model.id, email: pb.authStore.model.email, username: pb.authStore.model.username};
  }

  async logout() {
    await pb.authStore.clear();
  }

  async login({username, password}: {username: string, password: string}): Promise<void> {
    const t = await pb.collection("users").authWithPassword(username, password);
    if(!t) throw new Error("Invalid login");
  }
}
