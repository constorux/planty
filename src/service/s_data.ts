import PocketBase, { RecordModel } from 'pocketbase';

export interface Project{
    id: string;
    name: string;
    about?: string;
    floorplan?: string;
    width: number;
}

export interface Plant{
    id?: string;
    project: string;
    name: string;
    x_pos: number;
    y_pos: number;
    radius: number;
    type: "tree" | "bush" | "hedge" | "plant";
    state: "growing" | "removed" | "died";
    comment: string;

}

const pbURL = 'https://pocket.robbb.in';
export const pb = new PocketBase(pbURL);

export class DataService{
static readonly i = new DataService();

private constructor() {};

  
    // get projects
    async getProjects(userId: string): Promise<Project[]>{
        const projects = await pb.collection('planty_project').getList(1,500,{filter: `user = '${userId}'`});
        return projects.items.map((i) => i as any as Project).map((p) => ({...p, floorplan: p.floorplan ? (`${pbURL}/api/files/planty_project/${p.id}/${p.floorplan}`) :null}));
    }

    // get project
    async getProject(id: string): Promise<Project>{
        const project = (await pb.collection('planty_project').getOne(id))as any as Project;
        if(project.floorplan) project.floorplan = `${pbURL}/api/files/planty_project/${project.id}/${project.floorplan}`;
        return project;
    }

    // set project
    async setProject(id: string | null,changes: object): Promise<void>{
        if (id && changes instanceof FormData) {
            changes.delete("account");
          }
        !id ?
             await pb.collection('planty_project').create(changes)
        :   await pb.collection('planty_project').update(id, changes);
    }

    // delete project
    async deleteProject(projectId: string): Promise<void>{
        await pb.collection('planty_project').delete(projectId);
    }

    async getPlants(projectId: string): Promise<Plant[]>{
        const plants = await pb.collection('planty_plant').getList(1,500,{filter: `project = '${projectId}'`});
        return plants.items.map((i) => i as any as Plant);
    }

    async setPlant(plant: Plant): Promise<void>{
        !plant.id ?
             await pb.collection('planty_plant').create(plant)
        :   await pb.collection('planty_plant').update(plant.id, plant);
    }

    async createMany(plants:Plant[]): Promise<void>{
        await pb.collection('planty_plant').create(plants);
    }

    async deletePlant(plantId: string): Promise<void>{
        await pb.collection('planty_plant').delete(plantId);
    }

    async observePlants(id: string, onData: (plants: Plant[]) => void): Promise<void>{
        const project = (await pb.collection('planty_plant').subscribe("*",async () => onData(await this.getPlants(id)),{filter: `project = '${id}'`}));
        onData(await this.getPlants(id));
    }
}